# Stress Map

Prototype for a Stress Map projection that can be used in shaders globally.

 - less than 1ms CPU cost
 - less than 1ms on GPU
 - 1 texture (1MB, 4MB, 16MB depending on quality)
 - no GC allocations
 - localized, but can follow camera in open world environments, or have cascades

![](https://media.giphy.com/media/jp8lhuRf16teMkFE8r/giphy.gif)


## Requirements
Supported on DX11+ SM5.0, OpenGL 4.3+, OpenGL ES 3.1, Metal, Vulkan, PS4/XB1 consoles.


## Licence
This project is licensed under the MIT License - see the [LICENSE.md](https://gitlab.com/manwhocanfly/ai-demos/blob/master/LICENCE.md) file for details
