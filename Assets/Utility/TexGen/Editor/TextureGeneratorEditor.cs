using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(TextureGenerator), true, isFallback = false)]
public class TextureGeneratorEditor : Editor
{
    TextureGenerator texGen => (TextureGenerator)target;
    Color[] colorBuffer = new Color[0];

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();

        EditorGUILayout.Space();

        EditorGUILayout.BeginVertical(EditorStyles.helpBox);

        EditorGUILayout.LabelField("Texture Size", $"Width: {texGen.pixelSize.x}, Height: {texGen.pixelSize.y}");

        if (texGen.autoGenerateOnChange)
        {
            if (GUI.changed) Regenerate();
        }
        else if(GUILayout.Button("Generate") || texGen.texture == null)
        {
            Regenerate();
        }

        EditorGUILayout.EndVertical();
    }

    public void Regenerate()
    {
        var pixelSize = new Vector2Int(1 << texGen.width, 1 << texGen.height);

        var texture = texGen.texture;

        if (texture == null)
        {
            texture = new Texture2D(pixelSize.x, pixelSize.y, texGen.format, texGen.mipmap, texGen.linear);
        }

        texture.Resize(pixelSize.x, pixelSize.y, texGen.format, texGen.mipmap);
        texture.name = $"{texGen.name}";
        texture.filterMode = texGen.filterMode;

        var pixelCount = pixelSize.x * pixelSize.y;

        if (colorBuffer.Length != pixelCount) Array.Resize(ref colorBuffer, pixelCount);

        for (int x = 0; x < pixelSize.x; x++)
        {
            for (int y = 0; y < pixelSize.y; y++)
            {
                var id = x + y * pixelSize.x;
                var pixel = new Vector2Int(x, y);
                var uv = new Vector2((float)x / (pixelSize.x - 1), (float)y / (pixelSize.y - 1));

                colorBuffer[id] = texGen.GetPixel(id, pixel, uv);
            }
        }

        texture.SetPixels(colorBuffer);
        texture.Apply(true);

        if(texGen.texture == null)
        {
            texGen.texture = texture;

            AssetDatabase.AddObjectToAsset(texture, AssetDatabase.GetAssetPath(texGen));
        }

        EditorUtility.SetDirty(texture);
        AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(texGen), ImportAssetOptions.Default);
        AssetDatabase.Refresh();
    }

    public override bool HasPreviewGUI()
    {
        return texGen.texture != null;
    }

    public override void OnPreviewGUI(Rect r, GUIStyle background)
    {
        GUI.DrawTexture(r, texGen.texture, ScaleMode.ScaleToFit);
    }
}
