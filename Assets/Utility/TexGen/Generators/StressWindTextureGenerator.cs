﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StressWindTextureGenerator : TextureGenerator
{
    public override TextureFormat format => TextureFormat.RGBAHalf;

    [MinMaxRange(0f, 1f)]
    public RangedFloat falloff = new RangedFloat(0.35f, 0.95f);

    public Easing.Type easing = Easing.Type.Linear;

    public bool isCustomEasing => easing == Easing.Type.Custom;

    public AnimationCurve easingCurve;

    public override Color GetPixel(int id, Vector2Int pixel, Vector2 uv)
    {
        var pos = (uv - new Vector2(0.5f, 0.5f)) * 2f;

        var distance = Mathf.InverseLerp(falloff.minValue, falloff.maxValue, (1f - Mathf.Abs(pos.x)));
        distance = Easing.Evaluate(easing, Mathf.Clamp01(distance), easingCurve);

        return new Color(0f, 0f, distance, 0f);
    }
}
