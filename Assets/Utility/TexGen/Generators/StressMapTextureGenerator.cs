﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StressMapTextureGenerator : TextureGenerator
{
    public override TextureFormat format => TextureFormat.RGBAHalf;
    public override FilterMode filterMode => FilterMode.Point;

    [MinMaxRange(0f, 1f)]
    public RangedFloat falloff = new RangedFloat(0.35f, 0.95f);

    public Easing.Type easing = Easing.Type.Linear;

    public bool isCustomEasing => easing == Easing.Type.Custom;

    public AnimationCurve easingCurve;

    public float windStrenght = 0f;

    public override Color GetPixel(int id, Vector2Int pixel, Vector2 uv)
    {
        var pos = (uv - new Vector2(0.5f, 0.5f)) * 2f;

        var distance = Mathf.InverseLerp(falloff.minValue, falloff.maxValue, (1f - pos.magnitude));
        distance = Easing.Evaluate(easing, Mathf.Clamp01(distance), easingCurve);

        pos = pos.normalized * distance;

        return new Color(pos.x, pos.y, distance * windStrenght, Mathf.Sqrt(distance * windStrenght));
    }
}
