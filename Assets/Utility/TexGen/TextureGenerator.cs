﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TextureGenerator : ScriptableObject
{
    public bool autoGenerateOnChange = false;

    [SerializeField, Range(1, 11)]
    public int width = 5;

    [SerializeField, Range(1, 11)]
    public int height = 5;

    public Vector2Int pixelSize => new Vector2Int(1 << width, 1 << height);

    [HideInInspector]
    public Texture2D texture;

    public virtual TextureFormat format => TextureFormat.ARGB32;
    public virtual FilterMode filterMode => FilterMode.Bilinear;
    public virtual bool mipmap => false;
    public virtual bool linear => false;

    public virtual Color GetPixel(int id,  Vector2Int pixel, Vector2 uv)
    {
        return default(Color);
    }
}
