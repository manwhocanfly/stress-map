﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class WeightedListAbstract { }

public abstract class WeightedList<T, W> : WeightedListAbstract, IList<T> where T : WeightedValue<W>, new() {

	protected WeightedList() { }

	protected WeightedList(IEnumerable<W> items) {

		foreach (W item in items) {

			T t = new T {value = item, weight = 1};
			m_list.Add(t);
		}
	}

	protected WeightedList(IList<T> items) {

		m_list.AddRange(items);
	}

	[SerializeField]
	private List<T> m_list = new List<T>();

	public T this[int index] { get { return m_list[index]; } set { m_list[index] = value; } }

	public int Count => m_list.Count;
	public int Length => m_list.Count;

	public bool IsReadOnly => false;

	public IEnumerable<W> Values => m_list.Select(x => x == null ? default(W) : x.value); 

	public void Add(T item)
	{
		m_list.Add(item);
	}

	public void AddRange(IEnumerable<T> items)
	{
		m_list.AddRange(items);
	}

	public void Clear()
	{
		m_list.Clear();
	}

	public bool Contains(T item)
	{
		return m_list.Contains(item);
	}

	public void CopyTo(T[] array, int arrayIndex)
	{
		m_list.CopyTo(array, arrayIndex);
	}

	public IEnumerator<T> GetEnumerator()
	{
		return m_list.GetEnumerator();
	}

	public int IndexOf(T item)
	{
		return m_list.IndexOf(item);
	}

	public void Insert(int index, T item)
	{
		m_list.Insert(index, item);
	}

	public bool Remove(T item)
	{
		return m_list.Remove(item);
	}

	public void RemoveAt(int index)
	{
		m_list.RemoveAt(index);
	}

	IEnumerator IEnumerable.GetEnumerator()
	{
		return GetEnumerator();
	}

	public W GetRandom() {

		int selectedIndex = getRandomIndex();
		if (selectedIndex >= 0) {

			return m_list[selectedIndex];
		}

		return default(W);
	}

	public W RemoveRandom() {

		int selectedIndex = getRandomIndex();
		var rv = m_list[selectedIndex];

		m_list.RemoveAt(selectedIndex);

		return rv;
	}

	private int getRandomIndex() {

		int weight = 0;
		int count = m_list.Count;

		// calculate total weight
		for (int i = 0; i < count; i++) weight += m_list[i].weight;

		// get random position on weight line
		int randomValue = UnityEngine.Random.Range(0, weight);

		for (int i = 0; i < count; i++)
		{
			var c = m_list[i];

			weight -= c.weight;

			if (randomValue >= weight) return i;
		}

		return -1;
	}
}
