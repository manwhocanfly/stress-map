﻿using System;
using System.Linq;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.Text.RegularExpressions;
using UnityEditorInternal;

[CustomPropertyDrawer(typeof(WeightedValueAbstract), true)]
public class WeightedValueDrawer : PropertyDrawer
{
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		return EditorGUIUtility.singleLineHeight;
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginProperty(position, label, property);

		float weightPropertyWidth = 50f;

		float labelWidth = EditorGUIUtility.labelWidth;

		var spValue = property.FindPropertyRelative("value");
		var spWeight = property.FindPropertyRelative("weight");

		var vRect = new Rect(position.x, position.y + 1f, position.width - weightPropertyWidth - 4f, 16f);
		var wRect = new Rect(position.x + vRect.width + 4f, position.y + 1f, weightPropertyWidth, 16f);

		EditorGUI.PropertyField(vRect, spValue, label);

		EditorGUIUtility.labelWidth = 18f;

		spWeight.intValue = Mathf.Max(1, spWeight.intValue);
		EditorGUI.PropertyField(wRect, spWeight, new GUIContent("W", "Weight"));
		spWeight.intValue = Mathf.Max(1, spWeight.intValue);

		EditorGUIUtility.labelWidth = labelWidth;

		EditorGUI.EndProperty();
	}

}




[CustomPropertyDrawer(typeof(WeightedListAbstract), true)]
public class WeightedListDrawer : PropertyDrawer
{
	ReorderableList list;

	public override float GetPropertyHeight(SerializedProperty property, GUIContent label)
	{
		if (list == null) list = BuildReorderableList(property.FindPropertyRelative("m_list"), label);
		return list.GetHeight();
	}

	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label)
	{
		EditorGUI.BeginProperty(position, label, property);
		{
			if (list == null) list = BuildReorderableList(property.FindPropertyRelative("m_list"), label);

			list.DoList(position);
		}
		EditorGUI.EndProperty();
	}

	private ReorderableList BuildReorderableList(SerializedProperty property, GUIContent label)
	{
		ReorderableList list = new ReorderableList(property.serializedObject, property, true, true, true, true)
		{
			drawHeaderCallback = (Rect rect) =>
			{
				EditorGUI.LabelField(rect, label);
			},
			drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) =>
			{
				EditorGUI.PropertyField(rect, property.GetArrayElementAtIndex(index), true);
			}
		};
		return list;
	}
}