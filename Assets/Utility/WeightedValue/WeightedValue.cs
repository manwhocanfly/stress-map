﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public abstract class WeightedValueAbstract
{
	public int weight = 1;
}

public class WeightedValue<T> : WeightedValueAbstract
{
	public T value;

	public override string ToString()
	{
		return value.ToString();
	}

	public static implicit operator T(WeightedValue<T> wv)
	{
		return wv == null ? default(T) : wv.value;
	}
}