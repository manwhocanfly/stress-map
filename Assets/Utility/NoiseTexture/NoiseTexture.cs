﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class NoiseTexture : ScriptableObject
{
	public Texture2D texture;

	[Range(1,10)]
	public int size;

	[Serializable]
	public struct ChannelData
	{
		public float scale;
		public float offset;
	}

	public ChannelData channelR;
	public ChannelData channelG;
	public ChannelData channelB;
	public ChannelData channelA;
}
