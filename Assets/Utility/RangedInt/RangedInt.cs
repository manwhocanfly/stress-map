using System;

[Serializable]
public struct RangedInt {

	public int minValue;
	public int maxValue;
	public int midValue { get { return (minValue + maxValue) / 2; } }

	public int _currentValue;
	private bool _initialized;

	public RangedInt(int minValue, int maxValue) : this()
	{
		this.minValue = minValue;
		this.maxValue = maxValue;
	}

	public int value
    {
        get
        {
            if(!_initialized)
            {
                return Randomize();
            }

            return _currentValue;
        }
    }

    public int Randomize()
    {
        _initialized = true;
        _currentValue = UnityEngine.Random.Range(minValue, maxValue);

        return _currentValue;
    }

    public static implicit operator int (RangedInt ri)
    {
        return ri.value;
    }
}