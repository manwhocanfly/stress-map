﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StressMap
{
    public class StressMapMarker : MonoBehaviour
    {
        private static readonly int HNDL_Alpha = Shader.PropertyToID("_Alpha");

        internal GameObject markerObject { get; set; }
        private MaterialPropertyBlock materialOverride { get; set; }

        private float size = 10f;
        private float alpha = 1f;

        private void Awake()
        {
            materialOverride = new MaterialPropertyBlock();
        }

        private void OnEnable()
        {
            //StressMap.Markers.Add(this);

            //markerObject = StressMap.CreateNewMarker(transform.position);

            SetSize(10f);
            SetAlpha(1f);
        }
        
        private void OnDisable()
        {
            //StressMap.Markers.Remove(this);

            if (markerObject)
            {
                Destroy(markerObject);
                markerObject = null;
            }
        }

        public StressMapMarker SetSize(float size)
        {
            this.size = size;

            if (markerObject)
            {
                markerObject.transform.localScale = Vector3.one * size;
            }

            return this;
        }

        public StressMapMarker SetAlpha(float alpha)
        {
            this.alpha = alpha;

            if (markerObject)
            {
                materialOverride.Clear();
                if (alpha != 1f) materialOverride.SetFloat(HNDL_Alpha, alpha);
                markerObject.GetComponent<Renderer>().SetPropertyBlock(materialOverride);
            }

            return this;
        }
    }
}