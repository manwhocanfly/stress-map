﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StressMap
{
    [RequireComponent(typeof(Renderer))]
    public class StressMapDebug : MonoBehaviour
    {
        public enum Type { Camera, Stress, Velocity }

        public Type type = Type.Stress;

        private Material m_debugMaterial;

        public Material debugMaterial
        {
            get
            {
                if (m_debugMaterial == null)
                {
                    m_debugMaterial = new Material(Shader.Find("Unlit/Texture"));

                    switch (type)
                    {
                        case Type.Camera:
                            m_debugMaterial.SetTexture("_MainTex", StressMap.Main.cameraRT);
                            break;
                        case Type.Stress:
                            m_debugMaterial.SetTexture("_MainTex", StressMap.Main.stressMapRT);
                            break;
                        case Type.Velocity:
                            m_debugMaterial.SetTexture("_MainTex", StressMap.Main.stressMapVelocityRT);
                            break;
                    }

                }

                return m_debugMaterial;
            }
        }

        void Start()
        {
            GetComponent<Renderer>().material = debugMaterial;
        }
    }
}

