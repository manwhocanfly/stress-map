﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StressMap
{
    [RequireComponent(typeof(Camera))]
    public class StressMap : MonoBehaviour
    {
        public static StressMap Main { get; private set; }

        #region Shader Property Handles

        private int HNDL_RT; //render target
        private int HNDL_State; //current state of stress map
        private int HNDL_Velocity; //current delta of stress map 
        private int HNDL_Noise; //static 1D noise texture
        private int HNDL_Settings; //xy:position2D, zw:sizeInUnits2D
        private int HNDL_Time; //x:deltaTime, y:timeScale, z:1-timeScale, w:0
        private int HNDL_PixelSize; //map width and height in pixels
        private int HNDL_OffsetX; //delta x in pixels
        private int HNDL_OffsetY; //delta y in pixels
        private int HNDL_StressKernel; //compute shader kernel

        #endregion

        #region Serialized Properties

        [SerializeField]
        private string shaderPropertyPrefix = "_StressMap";

        [SerializeField, Tooltip("Keep it to the power of 2")]
        private int sizeInPixels = 32;

        [SerializeField]
        private float sizeInUnits = 1f;

        [SerializeField]
        private ComputeShader computeShader;

        [SerializeField]
        private Texture2D noiseTexture;

        #endregion

        #region Properties

        private Camera targetCamera;

        private Vector3 lastPosition;
        private Vector3 currentPosition;

        public RenderTexture cameraRT { get; private set; }
        public RenderTexture stressMapRT { get; private set; }
        public RenderTexture stressMapVelocityRT { get; private set; }

        private float unitsPerPixel => sizeInUnits / sizeInPixels;
        private int computeShaderGroups => (sizeInPixels + 7) / 8;

        #endregion

        private void Awake()
        {
            Main = this;
        }

        private void Start()
        {
            InitShaderPropertyHandles();
            CreateMap();
        }

        private void OnDestroy()
        {
            //Dispose texture
            cameraRT.DiscardContents();
            stressMapRT.DiscardContents();
            stressMapVelocityRT.DiscardContents();

            //Release resource
            cameraRT.Release();
            stressMapRT.Release();
            stressMapVelocityRT.Release();
        }

        private void OnPreRender()
        {
            transform.position = currentPosition = SnapToPixel(transform.position);
        }

        private void OnPostRender()
        {
            var time = new Vector4(Time.smoothDeltaTime, Time.timeScale, 1f - Time.timeScale, 0f);

            var deltaPosition = (currentPosition - lastPosition) / sizeInUnits;
            lastPosition = currentPosition;

            Shader.SetGlobalVector(HNDL_Settings, new Vector4(transform.position.x, transform.position.z, sizeInUnits, sizeInUnits));

            computeShader.SetVector(HNDL_Time, time);
            computeShader.SetInt(HNDL_OffsetX, Mathf.RoundToInt(deltaPosition.x * sizeInPixels));
            computeShader.SetInt(HNDL_OffsetY, Mathf.RoundToInt(deltaPosition.z * sizeInPixels));
            computeShader.Dispatch(HNDL_StressKernel, computeShaderGroups, computeShaderGroups, 1);
        }

        private void InitShaderPropertyHandles()
        {
            HNDL_RT = Shader.PropertyToID($"{shaderPropertyPrefix}RT");
            HNDL_State = Shader.PropertyToID($"{shaderPropertyPrefix}State");
            HNDL_Velocity = Shader.PropertyToID($"{shaderPropertyPrefix}Velocity");
            HNDL_Noise = Shader.PropertyToID($"{shaderPropertyPrefix}Noise");
            HNDL_Settings = Shader.PropertyToID($"{shaderPropertyPrefix}Settings");
            HNDL_Time = Shader.PropertyToID($"{shaderPropertyPrefix}Time");
            HNDL_PixelSize = Shader.PropertyToID($"{shaderPropertyPrefix}PixelSize");
            HNDL_OffsetX = Shader.PropertyToID($"{shaderPropertyPrefix}OffsetX");
            HNDL_OffsetY = Shader.PropertyToID($"{shaderPropertyPrefix}OffsetY");
            HNDL_StressKernel = computeShader.FindKernel($"Kernel");
        }

        private void CreateMap()
        {
            //Calculate properties
            lastPosition = currentPosition = transform.position;

            //Create textures
            cameraRT = CreateRenderTexture(false);
            stressMapRT = CreateRenderTexture(true);
            stressMapVelocityRT = CreateRenderTexture(true);

            //Set global textures
            Shader.SetGlobalTexture(HNDL_State, stressMapRT);
            Shader.SetGlobalTexture(HNDL_Velocity, stressMapVelocityRT);
            Shader.SetGlobalTexture(HNDL_Noise, noiseTexture);

            //Setup compute shader
            computeShader.SetTexture(HNDL_StressKernel, HNDL_RT, cameraRT);
            computeShader.SetTextureFromGlobal(HNDL_StressKernel, HNDL_State, HNDL_State);
            computeShader.SetTextureFromGlobal(HNDL_StressKernel, HNDL_Velocity, HNDL_Velocity);
            computeShader.SetInt(HNDL_PixelSize, sizeInPixels);

            //Setup camera
            targetCamera = GetComponent<Camera>();
            targetCamera.orthographicSize = sizeInUnits / 2f;
            targetCamera.targetTexture = cameraRT;

            //Initial position
            transform.position = lastPosition = SnapToPixel(transform.position);
        }

        private RenderTexture CreateRenderTexture(bool enableRandomWrite)
        {
            var texture = new RenderTexture(sizeInPixels, sizeInPixels, 0, RenderTextureFormat.ARGBHalf, RenderTextureReadWrite.Linear)
            {
                enableRandomWrite = enableRandomWrite
            };

            texture.Create();

            return texture;
        }

        private Vector3 SnapToPixel(Vector3 position)
        {
            return new Vector3
            (
                Mathf.Round(position.x / unitsPerPixel) * unitsPerPixel, 
                position.y, 
                Mathf.Round(position.z / unitsPerPixel) * unitsPerPixel
            );
        }



        //internal static StressMap Instance { get; set; }
        //internal static List<StressMapMarker> Markers { get; set; }

        //public struct ObjectMarkerLink
        //{
        //    public Transform target;
        //    public Transform marker;
        //}

        //internal static GameObject CreateNewMarker(Vector3 position)
        //{
        //    return Instantiate(DefaultMarkerPrefab, position, Quaternion.identity, Instance.transform);
        //}

        //private void Awake()
        //{
        //    Instance = this;
        //    DefaultMarkerPrefab = defaultMarkerPrefab;
        //    Markers = new List<StressMapMarker>();
        //}

        //private void LateUpdate()
        //{
        //    transform.position = targetCamera.transform.position + new Vector3(0f, 0f, 500f);

        //    for (int i = 0; i < Markers.Count; i++)
        //    {
        //        var marker = Markers[i];

        //        if (marker)
        //        {
        //            marker.markerObject.transform.position = marker.transform.position;
        //        }
        //        else
        //        {
        //            Markers.RemoveAt(i--);
        //        }
        //    }
        //}

        //public void CreateTemporaryMarker(Vector3 position, float startSize, float endSize, float time)
        //{
        //    StartCoroutine(CreateTemporaryMarkerCoroutine(position, startSize, endSize, time));
        //}

        //private IEnumerator CreateTemporaryMarkerCoroutine(Vector3 position, float startSize, float endSize, float duration)
        //{
        //    if (duration < 0f) yield break;

        //    var markerObject = new GameObject("TempStressMarker").GetComponent<Transform>();
        //    markerObject.position = position;
        //    var marker = markerObject.gameObject.AddComponent<StressMapMarker>();

        //    var startTime = Time.time;
        //    var t = 0f;

        //    while(t < 1f)
        //    {
        //        t = Mathf.Clamp01((Time.time - startTime) / duration);
        //        marker.SetSize(Mathf.Lerp(startSize, endSize, t));
        //        yield return null;
        //    }

        //    Destroy(markerObject.gameObject);
        //}
    }

}
