﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace StressGrass
{
    public class StressSprite : MonoBehaviour
    {
        public Vector3 distance;

        private void Update()
        {
            transform.position = Mathf.Sin(Time.time) * distance;    
        }

        private void OnWillRenderObject()
        {
            //if (StressCamera.main) transform.position = StressCamera.main.SnapToPixel(transform.position);
        }
    }
}

