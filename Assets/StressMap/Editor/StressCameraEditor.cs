using UnityEngine;
using UnityEditor;
using System;
using System.Collections;
using System.Collections.Generic;

namespace StressMap
{
    [CustomEditor(typeof(StressMap))]
    public class StressCameraEditor : Editor
    {
        StressMap instance => target as StressMap;

        public override bool HasPreviewGUI()
        {
            return instance.stressMapRT != null;
        }

        public override void OnPreviewGUI(Rect r, GUIStyle background)
        {
            GUI.DrawTexture(r, instance.stressMapRT, ScaleMode.ScaleToFit);
        }
    }
}
