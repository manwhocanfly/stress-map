﻿Shader "Unlit/GrassSprite"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Cutout("_Cutout", Range(0,1)) = 0.5
		_BendScale("_BendScale", Range(0,5)) = 1
	}
	SubShader 
	{
		Tags{ "Queue" = "Geometry" "RenderType" = "Transparent" }
		Blend SrcAlpha OneMinusSrcAlpha 

		Pass 
		{
			Tags { "LightMode" = "ForwardBase" }

			CGPROGRAM
			#include "UnityCG.cginc"

			#pragma target 4.5
			#pragma vertex vertex_shader
			#pragma fragment pixel_shader
			#pragma multi_compile_fwdbase

			sampler2D _StressMapState;
			float4 _StressMapSettings;
			sampler2D _StressMapNoise;

			half4 SampleStressMap(float3 worldPos)
			{
				float4 uv = float4
				(
					(worldPos.x - _StressMapSettings.x + _StressMapSettings.z / 2) / _StressMapSettings.z,
					(worldPos.z - _StressMapSettings.y + _StressMapSettings.z / 2) / _StressMapSettings.z,
					0,
					0
				);

				return tex2Dlod(_StressMapState, uv);
			}


			sampler2D _MainTex;
			float4 _MainTex_ST;
			float _Cutout;
			float _BendScale;

			struct Sprite
			{
				float3 position;
				float3 scale;

				float2 uvOffset;
				float2 uvSize;

				float stressMul;
				float windMul;
			};

			struct Vertex
			{
				float2 position;
				float2 uv;
			};

			StructuredBuffer<Sprite> Sprites;
			StructuredBuffer<Vertex> Vertices;
 
			struct v2p
			{
				float4 pos : SV_POSITION;
				float2 uv : TEXCOORD0;
			};

			v2p vertex_shader (uint id : SV_VertexID, uint inst : SV_InstanceID)
			{
				v2p vs;

				Sprite sprite = Sprites[inst];
				Vertex vertex = Vertices[id];

				float4 spriteWorldPos = float4(sprite.position * 2,1);
				float4 vertexWorldPos = float4(vertex.position.xyy * sprite.scale.xyy,1);
				vertexWorldPos.z *= 0.2;

				float4 worldPos = vertexWorldPos + spriteWorldPos;

				float4 stress = SampleStressMap(sprite.position);

				float stressForce = stress.x * stress.x + stress.y * stress.y;

				float4 noise = tex2Dlod(_StressMapNoise, float4(_Time.y * 0.5 + worldPos.x / 200, worldPos.z / 200,0,0));

				float2 turbulence = (noise.ba - 0.5) * (saturate(abs(stress.z)) * sign(stress.z));

				float stretchDistance = vertexWorldPos.y;

				worldPos.x += (stress.x + turbulence.x) * 1.5 * stretchDistance * _BendScale;
				worldPos.z += (stress.y + turbulence.y) * 0.5 * stretchDistance * _BendScale;
				worldPos.y -= stressForce * 1 * stretchDistance * _BendScale;
				worldPos.y -= stress.w * 3 * stretchDistance * _BendScale;

				vs.pos = mul(UNITY_MATRIX_VP, worldPos);
				vs.uv = sprite.uvOffset + sprite.uvSize * vertex.uv;

				return vs;
			}

			float4 pixel_shader (v2p i) : SV_Target
			{
				float4 c = tex2D(_MainTex, i.uv);
				clip(c.a - _Cutout);

				return c * UNITY_LIGHTMODEL_AMBIENT;
			}

			ENDCG
		}
	}
}
