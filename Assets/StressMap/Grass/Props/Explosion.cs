﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour
{
    new Renderer renderer;
    new Camera camera;
    Plane plane;

    IEnumerator Start()
    {
        plane = new Plane(Vector3.up, 0f);
        camera = Camera.main;
        renderer = GetComponent<Renderer>();

        while(true)
        {
            renderer.enabled = Input.GetMouseButton(0);
            yield return null;
        }
    }

    void Update()
    {
        var ray = camera.ScreenPointToRay(Input.mousePosition);

        if (plane.Raycast(ray, out float distance))
        {
            transform.position = ray.GetPoint(distance);
        }
    }
}
