﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.Rendering;

[Serializable]
public class WeightedSprite : WeightedValue<Sprite> { }

[Serializable]
public class WeightedSpriteList : WeightedList<WeightedSprite, Sprite> { }

public class GrassPropGenerator : MonoBehaviour
{
    [StructLayout(LayoutKind.Sequential)]
    struct GrassSprite
    {
        public Vector3 position;
        public Vector3 scale;

        public Vector2 uvOffset;
        public Vector2 uvSize;

        public float stressMul;
        public float windMul;
    }

    [StructLayout(LayoutKind.Sequential)]
    struct Vertex
    {
        public Vector2 position;
        public Vector2 uv;

        public Vertex(Vector2 position, Vector2 uv)
        {
            this.position = position;
            this.uv = uv;
        }
    }

    public Material floorMaterial;
    public Texture2D mask;

    public Vector2Int tiles = new Vector2Int(1, 1);

    public float size = 1f;
    public float sizeSpread = 0f;
    public Vector2 spacing = new Vector2(6f, 3f);
    public Vector2 randomOffset = new Vector2(2f, 2f);

    [Tooltip("If you use sprite sheet, put the number of columns here")]
    public Vector2 spriteScale = new Vector2(1f, 1f);
    public WeightedSpriteList sprites = new WeightedSpriteList();

    public Material grassMaterial;

    public GameObject aiPrefab;
    public int aiCount;

    Vector2 textureOffset;
    Vector2 textureScale;

    Rect bounds = new Rect(); // Using unity transform position X and Z

    List<Vector3> points = new List<Vector3>();

    Vertex[] vertexBufferData;
    ComputeBuffer vertexBuffer;

    GrassSprite[] spriteBufferData;
    ComputeBuffer spriteBuffer;

    CommandBuffer commandBuffer;
    Camera targetCamera;

    IEnumerator Start()
    {
        textureOffset = floorMaterial.GetTextureOffset("_MainTex");
        textureScale = floorMaterial.GetTextureScale("_MainTex");

        grassMaterial = Instantiate(grassMaterial);

        GeneratePoints();
        GenerateSprites();
        GenerateVertices();

        if (points.Count == 0)
        {
            Destroy(gameObject);
            yield break;
        }

        SetupBuffers();
        SetupMaterial();
        SetupCommandBuffer();

        for (int i = 0; i < aiCount; i++)
        {
            Instantiate(aiPrefab);
        }
    }

    private void OnRemove(Vector3 position, float radius)
    {
        var sqrRadius = radius * radius;

        var explosionPosition = new Vector2(position.x, position.z);
        var explosionRect = new Rect(position.x - radius, position.z - radius, radius * 2f, radius * 2f);

        if (bounds.Overlaps(explosionRect) == false) return;

        for (int i = 0; i < spriteBufferData.Length; i++)
        {
            var sprite = spriteBufferData[i];

            if (sprite.scale.x == 0f) continue;

            var spritePosition = new Vector2(sprite.position.x, sprite.position.z);

            if (explosionRect.Contains(spritePosition))
            {
                var distance = Mathf.Clamp01((explosionPosition - spritePosition).sqrMagnitude / sqrRadius);

                if (distance < 0.99f)
                {
                    sprite.scale = Vector3.zero;
                }

                spriteBufferData[i] = sprite;
            }
        }

        UpdateSpriteBuffers();
    }

    void OnDestroy()
    {
        if (vertexBuffer != null) vertexBuffer.Release();
        if (spriteBuffer != null) spriteBuffer.Release();
    }

    float GetValue(Vector3 position)
    {
        var uv = new Vector2(position.x, position.z);
        uv = Vector2.Scale(uv, textureScale) + textureOffset;
        return mask.GetPixelBilinear(uv.x, uv.y).r;
    }

    void GeneratePoints()
    {
        // To prevent infinite loop
        if (spacing.x <= Mathf.Epsilon || spacing.y <= Mathf.Epsilon) return;

        var minPosition = new Vector2();
        var maxPosition = new Vector2();

        points.Clear();

        for (int tileX = 0; tileX < tiles.x; tileX++)
        {
            for (int tileY = 0; tileY < tiles.y; tileY++)
            {
                var tile = new Vector3(tileX + 0.5f, 0, tileY + 0.5f) * 10f;

                var xOffset = (tile.y) % 3f;
                var start = new Vector3(Mathf.Ceil((tile.x - 5f - xOffset) / spacing.x) * spacing.x, tile.y, Mathf.Ceil((tile.z - 5f) / spacing.y) * spacing.y);
                var end = tile + new Vector3(5f, 0f, 5f);

                for (float x = start.x; x < end.x; x += spacing.x)
                {
                    for (float y = start.z; y < end.z; y += spacing.y)
                    {
                        var random = UnityEngine.Random.Range(0f, 1f);

                        var point = new Vector3(x, 0f, y);
                        point.x += UnityEngine.Random.Range(-randomOffset.x, randomOffset.x);
                        point.z += UnityEngine.Random.Range(-randomOffset.y, randomOffset.y);

                        points.Add(point);

                        if (points.Count == 1)
                        {
                            minPosition = maxPosition = new Vector2(point.x, point.z);
                        }
                        else
                        {
                            if (minPosition.x > point.x) minPosition.x = point.x;
                            if (minPosition.y > point.z) minPosition.y = point.z;

                            if (maxPosition.x < point.x) maxPosition.x = point.x;
                            if (maxPosition.y < point.z) maxPosition.y = point.z;
                        }
                    }
                }
            }
        }

        bounds = Rect.MinMaxRect(minPosition.x, minPosition.y, maxPosition.x, maxPosition.y);
    }

    void GenerateVertices()
    {
        vertexBufferData = new Vertex[]
        {
            new Vertex(new Vector2(0.5f, 0f), new Vector2(1f, 0f)),
            new Vertex(new Vector2(-0.5f, 0f), new Vector2(0f, 0f)),
            new Vertex(new Vector2(-0.5f, 1f), new Vector2(0f, 1f)),

            new Vertex(new Vector2(0.5f, 0f), new Vector2(1f, 0f)),
            new Vertex(new Vector2(-0.5f, 1f), new Vector2(0f, 1f)),
            new Vertex(new Vector2(0.5f, 1f), new Vector2(1f, 1f)),
        };
    }

    void GenerateSprites()
    {
        spriteBufferData = new GrassSprite[points.Count];

        for (int i = 0; i < points.Count; i++)
        {
            var sprite = sprites.GetRandom();
            var data = default(GrassSprite);

            var texSize = new Vector2(1f / sprite.texture.width, 1f / sprite.texture.height);
            var spriteRect = sprite.rect;

            data.uvOffset = Vector2.Scale(spriteRect.position, texSize);
            data.uvSize = Vector2.Scale(spriteRect.size, texSize);

            data.position = points[i];
            data.scale = Vector3.one * (size + UnityEngine.Random.Range(-sizeSpread, sizeSpread));
            data.scale.x *= data.uvSize.x * spriteScale.x;
            data.scale.y *= data.uvSize.y * spriteScale.y;

            data.stressMul = 1f;
            data.windMul = 1f;

            spriteBufferData[i] = data;
        }
    }

    void SetupBuffers()
    {
        spriteBuffer = new ComputeBuffer(spriteBufferData.Length, Marshal.SizeOf<GrassSprite>(), ComputeBufferType.Default);
        spriteBuffer.SetData(spriteBufferData);

        vertexBuffer = new ComputeBuffer(vertexBufferData.Length, Marshal.SizeOf<Vertex>(), ComputeBufferType.Default);
        vertexBuffer.SetData(vertexBufferData);
    }

    void SetupMaterial()
    {
        grassMaterial.SetTexture("_MainTex", sprites.Values.FirstOrDefault()?.texture);
        grassMaterial.SetBuffer("Vertices", vertexBuffer);
        grassMaterial.SetBuffer("Sprites", spriteBuffer);
    }

    private void SetupCommandBuffer()
    {
        commandBuffer = new CommandBuffer
        {
            name = "Draw Grass Sprites"
        };

        commandBuffer.DrawProcedural(transform.localToWorldMatrix, grassMaterial, 0, MeshTopology.Triangles, vertexBufferData.Length, spriteBufferData.Length);
    }

    void OnRenderObject()
    {
        grassMaterial.SetPass(0);
        Graphics.DrawProcedural(MeshTopology.Triangles, vertexBufferData.Length, spriteBufferData.Length);
    }

    void UpdateSpriteBuffers()
    {
        spriteBuffer.SetData(spriteBufferData);
    }
}