﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicAI : MonoBehaviour
{
    Vector3 targetPoint;

    float random;

    private void Awake()
    {
        random = Random.Range(2f, 3f);
        targetPoint = GetNewTarget();

        transform.position = GetNewTarget();
        transform.localScale = Random.Range(1f, 3f) * 4f * Vector3.one;
    }

    private void FixedUpdate()
    {
        var delta = targetPoint - transform.position;
        var force = delta.normalized * 15f * random;
        GetComponent<Rigidbody>().AddForce(force, ForceMode.Force);

        if (delta.sqrMagnitude < 10f) targetPoint = GetNewTarget();
    }

    Vector3 GetNewTarget()
    {
        return new Vector3(Random.Range(110f, 290f), 0f, Random.Range(10f, 290f));
    }
}
